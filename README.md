## Requirements

| Prerequisite    | How to check | How to install
| --------------- | ------------ | ------------- |
| NVM >12.18.4  | `nvm list`    | [NVM](https://github.com/coreybutler/nvm-windows), `nvm install 12.18.4`, this will handle your NODE and NPM packages for you! |
| PHP >= 7.x.x    | `php -v`     | [php.net](http://php.net/manual/en/install.php), if not using a local environment tool|
| MySql >= 5.6.x  |  ---   | --- |

## Features
* [Webpack 5](https://webpack.js.org/), bundling assets
* [Bootstrap 5](https://getbootstrap.com/), front-end framework
* [Dart Sass](https://sass-lang.com/), Sass VM for almost instantaneous scss updates

### Additional Features that are availble if needed!
* [Axios](https://github.com/axios/axios), Promise based HTTP client for the browser and node.js
* [Vue](https://github.com/vuejs/vue),  A progressive, incrementally-adoptable JavaScript framework for building UI on the web.
* [AOS Scroll](https://github.com/michalsnik/aos),  Animate on Scroll
* Linting is using Airbnb-base and Vue-Strongly-recommended, using VsCode, when asked if you want to allow the use of the linting file over your plugin, please select yes ( Allow ).

## Documentation

### Do you have SASS VM installed locally?
* [Dart Sass VM](https://github.com/sass/dart-sass/releases/tag/1.37.5)

### Change your proxy url
* build > webpack.config.js > BrowserSyncPlugin > update to use your currently proxy URL for your local install

### From the command line
* cd into theme directory
    * npm: `npm ci`
        * will install all your npm packages based on your package-lock and package.json matching up
    * watch: `npm run watch`
        * builds dev assets to public directory and watches assets for changes!
        * sets up browsersync
	* devsass: `npm run devsass`
		* runs sass watch form common.css
		* additional files can be added if needed
    * dev: `npm run dev`
        * builds dev assets to public directory
    * prod: `npm run prod`
        * builds production assets to public directory
		* runs production sass as well after webpack compiles js/images/fonts

### Assets Folder
* scripts / css / fonts / images will go inside the /assets directory
    * You'll see multiple folders inside of /assets containing said files
* JS will consist of separate entry points set in webpack.

### Using BrowserSync
* To use BrowserSync during `npm run watch` you need to update `proxy` near the bottom of `webpack/webpack.config.js` to reflect your local development hostname.
* You'll want to open a 2nd terminal to run `npm run devsass` along side of the watch command.

# [Sage](https://roots.io/sage/)

## Documentation

Sage documentation is available at [https://roots.io/sage/docs/](https://roots.io/sage/docs/).
