/**
 * Internal application javascript files.
 * Supports ES6 by compiling scripts with Babel.
 */
module.exports = {
	test: /\.js$/,
	exclude: /node_modules/,
	// exclude: /node_modules\/(?!(x5-gmaps|vue-google-autocomplete|vue-accessible-multiselect)\/).*/,
	use: [
		{
			loader: 'babel-loader',
			options: {
				presets: [
					[
						'@babel/preset-env',
						{
							targets: {
								browsers: ['last 2 versions', 'ie >= 11'],
							}
						}
					],
				]
			}
		},
		{
			loader: 'eslint-loader',
			options: {
				failOnError: true,
			},
		}
	],
};
