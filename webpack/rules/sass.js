const autoprefixer = require('autoprefixer');
const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const config = require('../app.config');

module.exports = {
	rules: [{
		test: /\.s[ac]ss$/,
		exclude: /node_modules/,
		include: config.paths.sass,
		use: [
			MiniCssExtractPlugin.loader,
			{
				loader: 'css-loader',
				options: { sourceMap: true },
			},
			{
				loader: 'postcss-loader',
				options: {
					postcssOptions: {
						config: path.resolve(__dirname, '../postcss.config.js'),
					},
				}
			},
			{
				loader: 'sass-loader',
				options: {
					// sourceMap: false
				}
			}
		]
	}]
};
