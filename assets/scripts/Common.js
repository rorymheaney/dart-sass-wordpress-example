// bootstrap 5 latest iteration
// https://getbootstrap.com/docs/5.0/getting-started/introduction/
// import 'bootstrap';

// owl
// import 'owl.carousel';

// jquery cookie
// import 'js-cookie';
// window.fancySquareCookies = require('js-cookie');

// lazy load images and bg's
import 'lazysizes';
import 'lazysizes/plugins/respimg/ls.respimg';
import 'lazysizes/plugins/bgset/ls.bgset';

const UTIL_JS = require('./modules/Utility');

// set breakpoint on page load, breakpoints match media query variables - xxl, xl, lg, etc
window.bsBreakpoint = UTIL_JS.returnBootstrapBreakpoints();

// axios
window.axios = require('axios');

// alert('what');
