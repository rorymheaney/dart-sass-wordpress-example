<?php
		// header options
		if ( have_rows( 'header_flexible_content', $postID ) ) :

			while ( have_rows( 'header_flexible_content', $postID ) ) : the_row();

			get_template_part( 'templates/flexible-content/header/' . get_row_layout(), null, '');

			endwhile;

		elseif ( get_the_content() ) :
			// no layouts found
			echo 'Select A layout';
		endif;
	?>

<?php
	// get main content
	if ( have_rows( 'body_flexible_content', $postID ) ) :
		$count = 0;
		// loop through the selected ACF layouts and display the matching partial
		while ( have_rows( 'body_flexible_content', $postID ) ) : the_row();
			// increase counter per loop
			// count will be put on the href
			$count++;
			get_template_part( 'templates/flexible-content/main/' . get_row_layout(), null, array(
				'index' => $count
			) );

		endwhile;

	elseif ( get_the_content() ) :
		// no layouts found
		echo 'Select A layout';
	endif;
?>
