<footer class="content-info">
	<div class="container">
		<?php //dynamic_sidebar('sidebar-footer'); ?>
		© <?php _e(date('Y')); ?> <?php bloginfo('name'); ?>, All Rights Reserved.
	</div>
</footer>
